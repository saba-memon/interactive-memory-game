﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409
//breaks program: when image that has already been revealed or paired is tapped
//Created by: Saba Memon

namespace Interactive_Memory_Game
{
    /// <summary>
    /// Main page displays images that can be clicked on.
    /// User can match pairs of images which will display the image match at the bottom.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //create images
        BitmapImage _Tile1pair;
        BitmapImage _Tile2pair;
        BitmapImage _Tile3pair;
        BitmapImage _Tile4pair;
        BitmapImage _Smiley;
        BitmapImage _Question;

        int _numTaps = 0; // initialize _numTaps to later check how many images revealed

        public MainPage()
        {
            this.InitializeComponent();
            
            //set images as assets
            _Tile1pair = new BitmapImage(new Uri("ms-appx:///Assets/Tile1.jpg"));
            _Tile2pair = new BitmapImage(new Uri("ms-appx:///Assets/Tile2.jpg"));
            _Tile3pair = new BitmapImage(new Uri("ms-appx:///Assets/Tile3.jpg"));
            _Tile4pair = new BitmapImage(new Uri("ms-appx:///Assets/Tile4.gif"));
            _Smiley = new BitmapImage(new Uri("ms-appx:///Assets/Smiley.jpg"));
            _Question = new BitmapImage(new Uri("ms-appx:///Assets/QuestionMark.jpg"));
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile1Tapped(object sender, TappedRoutedEventArgs e)
        {
            _imgTile1.Source = _Tile1pair; //set image source
            if (_imgTile1.Source == _Tile1pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            } //if ends

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile1.Source == _imgTile7.Source) //should both be _Tile1pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!");//message displays for matched pair
                    await msg.ShowAsync();

                    _imgMatch1.Visibility = Visibility.Visible; // display the image match 
                    _imgTile1.Source = _Smiley;
                    _imgTile7.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile1.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                } //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                    await msg.ShowAsync();
            } //if ends
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile2Tapped(object sender, TappedRoutedEventArgs e)
        {
            _imgTile2.Source = _Tile3pair; //set image source
            if (_imgTile2.Source == _Tile3pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            }

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile2.Source == _imgTile8.Source) //should both be _Tile3pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!"); //message displays for matched pair
                    await msg.ShowAsync();
                    _imgMatch3.Visibility = Visibility.Visible; // display the image match 
                    _imgTile2.Source = _Smiley;
                    _imgTile8.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile2.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                } //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                await msg.ShowAsync();
            } //if ends
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile3Tapped(object sender, TappedRoutedEventArgs e)
        {
            _imgTile3.Source = _Tile2pair; //set image source
            if (_imgTile3.Source == _Tile2pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            } //if ends

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile3.Source == _imgTile5.Source) //should both be _Tile2pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!"); //message displays for matched pair
                    await msg.ShowAsync();
                    _imgMatch2.Visibility = Visibility.Visible; // display the image match 
                    _imgTile3.Source = _Smiley;
                    _imgTile5.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile3.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                } //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                await msg.ShowAsync();
            } //if ends
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile4Tapped(object sender, TappedRoutedEventArgs e)
        {
            _imgTile4.Source = _Tile4pair; //set image source
            if (_imgTile4.Source == _Tile4pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            } //if ends

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile4.Source == _imgTile6.Source) //should both be _Tile4pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!");
                    await msg.ShowAsync();
                    _imgMatch4.Visibility = Visibility.Visible; // display the image match 
                    _imgTile4.Source = _Smiley;
                    _imgTile6.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile4.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                }  //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                await msg.ShowAsync();
            } //if ends
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile5Tapped(object sender, TappedRoutedEventArgs e) //pair6 = pair2
        {
            _imgTile5.Source = _Tile2pair; //set image source
            if (_imgTile5.Source == _Tile2pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            } //if ends

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile5.Source == _imgTile3.Source) //should both be _Tile2pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!"); //message displays for matched pair
                    await msg.ShowAsync();
                    _imgMatch2.Visibility = Visibility.Visible; // display the image match 
                    _imgTile3.Source = _Smiley;
                    _imgTile5.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile5.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                }  //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                await msg.ShowAsync();
            } //if ends
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile6Tapped(object sender, TappedRoutedEventArgs e)//pair8 = pair4
        {
            _imgTile6.Source = _Tile4pair; //set image source
            if (_imgTile6.Source == _Tile4pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            } //if ends

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile6.Source == _imgTile4.Source) //should both be _Tile4pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!"); //message displays for matched pair
                    await msg.ShowAsync();
                    _imgMatch4.Visibility = Visibility.Visible; // display the image match 
                    _imgTile4.Source = _Smiley;
                    _imgTile6.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile6.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                } //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                await msg.ShowAsync();
            } //if ends
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile7Tapped(object sender, TappedRoutedEventArgs e)//pair5=pair1
        {
            _imgTile7.Source = _Tile1pair; //set image source
            if (_imgTile7.Source == _Tile1pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            } //if ends

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile1.Source == _imgTile7.Source) //should both be _Tile1pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!"); //message displays for matched pair
                    await msg.ShowAsync();

                    _imgMatch1.Visibility = Visibility.Visible; // display the image match 
                    _imgTile1.Source = _Smiley;
                    _imgTile7.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile7.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                } //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                await msg.ShowAsync();
            } //if ends
        }

        /// <summary>
        /// Changes the source of the tile tapped.
        /// Checks if a pair is matched or if all tiles have been paired.
        /// </summary>
        private async void OnTile8Tapped(object sender, TappedRoutedEventArgs e)//pair7=pair3
        {
            _imgTile8.Source = _Tile3pair; //set image source
            if (_imgTile8.Source == _Tile3pair) //if source of tile matches its pair
            {
                _numTaps += 1; //increment _numTaps
            } //if ends

            if (_numTaps == 2) //checks if two tiles have been revealed
            {
                _numTaps = 0; //reset _numTaps because it equals 2
                if (_imgTile8.Source == _imgTile2.Source) //should both be _Tile3pair
                {
                    MessageDialog msg = new MessageDialog("You have a match!"); //message displays for matched pair
                    await msg.ShowAsync();
                    _imgMatch3.Visibility = Visibility.Visible; // display the image match 
                    _imgTile2.Source = _Smiley;
                    _imgTile8.Source = _Smiley;
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Sorry, the tiles do not match. Please try again.");
                    await msg.ShowAsync();
                    _imgTile8.Source = _Question; // revert the image displayed to the question mark
                    _numTaps = 1; //one image still not flipped, user can try another match!
                } //if ends
            } //if ends

            if (_imgTile1.Source == _Smiley && // check if all tiles paired
                _imgTile2.Source == _Smiley &&
                _imgTile3.Source == _Smiley &&
                _imgTile4.Source == _Smiley &&
                _imgTile5.Source == _Smiley &&
                _imgTile6.Source == _Smiley &&
                _imgTile7.Source == _Smiley &&
                _imgTile8.Source == _Smiley)
            { // display a success message
                MessageDialog msg = new MessageDialog("You have completed the game. Congratulations!");
                await msg.ShowAsync();
            } //if ends
        }
    }
}
